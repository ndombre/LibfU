#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *c, size_t number)
{
	char *t1 = strdup(c);
	char *t2 = strdup(c);
	bzero((void*)t1, number);
	ft_bzero((void*)t2, number);
	if (memcmp(t1, t2, number) != 0)
		printf("ERROR use(%s)\n", c);
	free(t1);
	free(t2);
}

int	main()
{
	test("almost every programmer should know memset !!!", 6);
	test("almost every programmer should know memset !!!", 6);
	test("almost every programmer should know memset !!!", 0);
	test("almost every programmer should know memset !!!", sizeof("almost every programmer should know memset !!!"));
	test("a", sizeof("a"));
	test("Hello !!", sizeof("Hello !!"));
	test("Hello !!", sizeof("Hello !!"));
}
