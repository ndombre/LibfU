#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*	$OpenBSD: strlcat.c,v 1.12 2005/03/30 20:13:52 otto Exp $	*/

/*
 * Copyright (c) 1998 Todd C. Miller <Todd.Miller@courtesan.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

//#include <sys/types.h>
//#include <string.h>

/*
 * Appends src to string dst of size siz (unlike strncat, siz is the
 * full size of dst, not space left).  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz <= strlen(dst)).
 * Returns strlen(src) + MIN(siz, strlen(initial dst)).
 * If retval >= siz, truncation occurred.
 */
size_t ssstrlcat(char *dst, const char *src, size_t siz)
{
	char *d = dst;
	const char *s = src;
	size_t n = siz;
	size_t dlen;

	/* Find the end of dst and adjust bytes left but don't go past end */
	while (n-- != 0 && *d != '\0')
		d++;
	dlen = d - dst;
	n = siz - dlen;

	if (n == 0)
		return(dlen + strlen(s));
	while (*s != '\0') {
		if (n != 1) {
			*d++ = *s;
			n--;
		}
		s++;
	}
	*d = '\0';

	return(dlen + (s - src));	/* count does not include NUL */
}

void	test(char const *b, char *c, size_t n)
{
	char *b1 = malloc(sizeof(char) * (strlen(b) + strlen(c) -1));
	char *b2 = malloc(sizeof(char) * (strlen(b) + strlen(c) -1));
	strcpy(b1, b);
	strcpy(b2, b);
	size_t a1 = ft_strlcat(b1, c, n);
	size_t a2 = strlcat(b2, c, n);
	if (a1 != a2)
		printf("ERROR use1(%s|%s|%s|%s(%ld %ld))\n", c, b, b1, b2, a1, a2);
	if (strcmp(b1, b2) != 0)
		printf("ERROR use1(%s|%s|%s|%s(%ld %ld))\n", c, b, b1, b2, a1, a2);
	free(b1);
	free(b2);
}

int	main()
{
	test("tes1", "est1", 2);
	test("tes2", "est2", strlen("test") + strlen("test"));
	test("tes2", "est2", strlen("test") + strlen("test")+10);
	test("f", "f", 2);
	test("", "f", 2);
	test("f", "", 2);
	test("te\0  \t\tst", "gfdsohgfojdhsflhsdjfghdksgfkdsgfksd", 150);
	test("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl", "4", 5);
	int i;
	for (i=0; i< 500; i++)
		test("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl", "4fdklhjfkdljhkljfdgkmfdndnvjrjghljsdfhglkhfgkshdlksfndslnflkndslkfndknlfldsf", i);
	for (i=0; i< 500; i++)
		test("DOFHHJGFLDS:Hfjfdklshfjl", "4fdklhjfkdljhkljfdgkmfdndnvjrjghljsdfhglkhfgkshdlksfndslnflkndslkfndknlfldsf", i);
}
