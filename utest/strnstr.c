/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/11 18:43:58 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/08 13:45:16 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include "../libft.h"

void find_str(char* str, char* substr, size_t n)
{
	char* pos = ft_strnstr(str, substr, n);
	char* posreel = strnstr(str, substr, n);
	if (pos != posreel)
	{
		if(pos) {
	    		printf("ERROR found the string '%s' in '%s' at position: %ld  --  %ld  %ld\n", substr, str, pos - str, posreel - str, n);
		} else {
			printf("ERROR the string '%s' was not found in '%s'\n", substr, str);
		}
	}
}
 
int main(void) 
{
	char* str = "one two three";
	find_str(str, "two", 0);
	find_str(str, "two", 1);
	find_str(str, "two", 5);
	find_str(str, "two", 6);
	find_str(str, "two", 7);
	find_str(str, "two", 8);
	find_str(str, "two", 9);
	find_str(str, "two", strlen(str));
	find_str(str, "", strlen(str));
	find_str(str, "nine", strlen(str));
	find_str(str, "n", strlen(str));
	find_str(str, "y", strlen(str));
	find_str(str, "one two threeeee", strlen(str));
	find_str(str, "", strlen(str));
	find_str("", "", strlen(str));
	find_str("", "two", strlen(str));
	find_str("test fd dwd saf sfsf.", ".", 100);

	return 0;
}
