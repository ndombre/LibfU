#include "../libft.h"
#include "string.h"
#include <stdio.h>

void	test(const char *c, const char *c2)
{
	if (strcmp(c, c2) != ft_strcmp(c, c2))
		printf("ERROR use(%d|%d:%s|%s)\n", strcmp(c, c2), ft_strcmp(c, c2), c, c2);
	if (ft_strequ(c, c2) != (strcmp(c, c2)==0))
		printf("error\n");
	if (ft_strnequ(c, c2, 5) != (memcmp(c, c2, 5)==0))
		printf("error\n");
}

int	main()
{
	test("", "");
	test("test", "test");
	test("test42test42testtesttest424242testtesttesttesttest4222222testtest4242\t\vfds", "test42test42testtesttest424242testtesttesttesttest4222222testtest4242\t\vfds");

	test("", "1");
	test("1", "");
	test("test", "tes1");
	test("test42test42testtesttest424242testtesttesttettest4222222testtest4242\t\vfds", "test42test42testtesttest424242testtesttesttesttest4222222testtest4242\t\vfds");
	test("\200", "");
	char t = -2;
	test(&t, "");
}
