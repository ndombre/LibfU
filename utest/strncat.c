#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *a, char *b, size_t n)
{
	size_t nm = strlen(a) + n;
	char *an = malloc(nm);
	strcpy(an, a);
	char *am = malloc(nm);
	strcpy(am, a);
	if (strcmp(strncat(an, b, n), ft_strncat(am, b, n)) != 0)
		printf("error\n");
	if (strcmp(an, am) != 0)
		printf("error normal:((%s)) ft_((%s))\n", an, am);
	free(an);
	free(am);
}

void	testn(char *a, char *b, size_t n, size_t m)
{
	char *an = malloc(m);
	strcpy(an, a);
	char *am = malloc(m);
	strcpy(am, a);
	if (strcmp(strncat(an, b, n), ft_strncat(am, b, n)) != 0)
		printf("error\n");
	if (strcmp(an, am) != 0)
		printf("error normal:((%s)) ft_((%s))\n", an, am);
	free(an);
	free(am);
}

int		main(void)
{
	test("qwertyuioplkjhgfdsazxcvbnm,.", "mnbvcxz1234567890", 5);
	test("qwertyuioplkjhgfdsazxcvbnm,.", "mnbvcxz1234567890", 17);
	test("qwertyuioplkjhgfdsazxcvbnm,.", "mnbvcxz1234567890", 100);
	test("qwertyuioplkjhgfdsazxcvbnm,.", "mnbvcxz1234567890", 1);
	test("qwertyuioplkjhgfdsazxcvbnm,.", "mnbvcxz1234567890", 0);
	test("qwert", "567890", 1000);
	test("", "567890", 1000);
	test("qwert", "", 1000);
	testn("trewsdfgtr", "res", 1000, 15);
}
