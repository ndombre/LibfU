/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 18:32:12 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 11:35:55 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>
#include <stdlib.h>

void	df(void* pt, size_t s)
{
	if (s != 0)
		printf("error4\n");
	if (pt != NULL)
		printf("error23\n");
}

int		main(void)
{
	t_list	*l;

	l = ft_lstnew(NULL, ft_strlen("test") + 1);
	if (l->content_size != 0)
		printf("error1\n");
	ft_lstdelone(&l, df);
	if (l != NULL)
		printf("error2\n");
}
