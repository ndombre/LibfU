#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *c, int set, size_t number)
{
	char *t1 = strdup(c);
	char *t2 = strdup(c);
	if (strcmp(memset((void*)t1, set, number), ft_memset((void*)t2, set, number)) != 0)
		printf("ERROR use(%s|%s:%s)\n", (char*)memset(t1, set, number), (char*)ft_memset(t2, set, number), c);
	if (strcmp(t1, t2) != 0)
		printf("ERROR use(%s|%s:%s)\n", t1, t2, c);
	if (memcmp(t1, t2, number) != 0)
		printf("ERROR use(%s|%s:%s)\n", t1, t2, c);
	free(t1);
	free(t2);
}

int	main()
{
	test("almost every programmer should know memset !!!", '-', 6);
	test("almost every programmer should know memset !!!", 2, 6);
	test("almost every programmer should know memset !!!", 2, 0);
	test("almost every programmer should know memset !!!", 'a', sizeof("almost every programmer should know memset !!!"));
	test("a", '\0', sizeof("a"));
	test("Hello !!", 1024, sizeof("Hello !!"));
	test("Hello !!", -5, sizeof("Hello !!"));
}
