#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *c, char f, size_t number)
{
	void *dest1 = malloc(sizeof(char) * number);
	void *dest2 = malloc(sizeof(char) * number);

	void *r1 = ft_memccpy(dest1, c, f, number);
	void *r2 = memccpy(dest2, c, f, number);

	if (r1 - dest1 != r2 - dest2 && r1 != NULL)
	{
		printf("ERROR use1(%s(%c)%lu, %lu)\n", c, f, r1 - dest1, r2 - dest2);
	}
	if ((r1 == NULL && r2 != NULL) || (r1 != NULL && r2 == NULL) || (r1 != NULL && strcmp(r1, r2) != 0))
		printf("ERROR use1(%s(%c)%lu, %lu)\n", c, f, r1 - dest1, r2 - dest2);
	if (memcmp(dest1, dest2, number) != 0)
		printf("ERROR use2(%s(%c), %s, %s)\n", c, f, (char*)dest1, (char*)dest2);
}

int	main()
{
	test("test", 'e', sizeof ("test"));
	test("test", 't', sizeof ("test"));
	test("test", 's', sizeof ("test"));
	test("test", '\0', sizeof ("test"));
	test("test", 'q', sizeof ("test"));
	test("", '\0', sizeof (""));
}
