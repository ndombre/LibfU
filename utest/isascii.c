#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

void	test(int c)
{
	if ((isascii(c) != 0) != (ft_isascii(c) != 0))
		printf("ERROR use(%d|%d:%d)\n", isascii(c), ft_isascii(c), c);
}

int	main()
{
	int i = -5;
	while (i <= 255)
	{
		test(i);
		i++;
	}
}
