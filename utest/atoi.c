/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/11 10:20:15 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/07 15:53:09 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int		ft_putnbr(int nb);
int		ft_atoi(char *str);

int		ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

void		test(char *str)
{
	if (ft_atoi(str) != atoi(str))
	{
		printf("%s\t%d:%d\n", str, ft_atoi(str), atoi(str));
	}
}

int		main(void)
{
	test("33");
	test("-43");
	test("+43");
	test(" -43");
	test("- 43");
	test("-4 3");
	test("-43");
	test("-43e");
	test("+-4 3");
	test("-+4 3");
	test("");
	test("2147483647");
	test("–2147483648");
	test("--3");
	test("++5");
	test("54352147483648");
	test("\t54");
	test("   543");
	test("\n543");
	test("(543");
	test("!543");
	test("5'43");
	test("54.3");
	test("5*43");
	test("\v543");
	test("5\v43");
	test("\r543");
	test("5\r43");
	return (0);
}
