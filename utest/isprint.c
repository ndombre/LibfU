#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

void	test(int c)
{
	if ((isprint(c)!=0) != (ft_isprint(c)!=0))
		printf("ERROR use(%d|%d:%d)\n", isprint(c), ft_isprint(c), c);
}

int	main()
{
	int i = -5;
	while (i <= 255)
	{
		test(i);
		i++;
	}
}
