#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char const *b, char *c)
{
	char *b1 = malloc(sizeof(char) * (strlen(b) + strlen(c) -1));
	char *b2 = malloc(sizeof(char) * (strlen(b) + strlen(c) -1));
	strcpy(b1, b);
	strcpy(b2, b);
	if (strcmp(ft_strcat(b1, c), strcat(b2, c)) != 0)
		printf("ERROR use1(%s|%s|%s|%s)\n", c, b, b1, b2);
	if (strcmp(b1, b2) != 0)
		printf("ERROR use2(%s)\n", c);
	free(b1);
	free(b2);
}

int	main()
{
	test("test", "este");
	test("f", "f");
	test("te\0  \t\tst", "gfdsohgfojdhsflhsdjfghdksgfkdsgfksd");
	test("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl", "4");
}
