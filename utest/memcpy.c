#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *c, size_t number)
{
	void *dest = malloc(sizeof(char) * number);
	if (memcmp(ft_memcpy(dest, c, number), c, number) != 0)
		printf("ERROR use(%s)\n", c);
	if (memcmp(dest, c, number) != 0)
		printf("ERROR use(%s)\n", c);
}

int	main()
{
	test("test", sizeof ("test"));
	test("", 0);
	test("te\0  \t\tst", sizeof ("te\0  \t\tst")+1);
	test("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl", sizeof ("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl")+1);
}
