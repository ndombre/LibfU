#include "../libft.h"
#include <string.h>
#include <stdio.h>

int main ()
{
	char * pch;
	char str[] = "Example string";
	pch = (char*) ft_memchr (str, 'p', strlen(str));
	if (pch!=NULL && pch-str+1 == 5)
	{
		//printf ("'p' found at position %d.\n", pch-str+1);
	}
	else
	{
		printf ("ERROR\n");
	}
	return 0;
}
