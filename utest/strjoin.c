#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char const *b, char *c)
{
	char *b2 = malloc(sizeof(char) * (strlen(b) + strlen(c) -1));
	strcpy(b2, b);
	char *r = ft_strjoin(b, c);
	if (strcmp(r, strcat(b2, c)) != 0)
		printf("ERROR use1(%s|%s|%s)\n", c, b, b2);
	free(b2);
	free(r);
}

int	main()
{
	test("test", "este");
	test("f", "f");
	test("te\0  \t\tst", "gfdsohgfojdhsflhsdjfghdksgfkdsgfksd");
	test("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl", "4");
	if (strcmp(ft_strjoin("", ""), "") != 0)
		printf("error\n");
}
