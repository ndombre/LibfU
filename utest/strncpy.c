#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *c, size_t n)
{
	void *dest = malloc(sizeof(char) * n + 100);
	void *dest2 = malloc(sizeof(char) * n + 100);
	memset(dest, '-', n + 100);
	memset(dest2, '-', n + 100);
	((char*)dest)[n + 1] = '\0';
	((char*)dest2)[n + 1] = '\0';
	if (strcmp(ft_strncpy(dest, c, n), strncpy(dest2, c, n)) != 0)
		printf("ERROR use1(%s\n%s\n%s)\n", c, dest, dest2);
	if (strcmp(dest, dest2) != 0)
		printf("ERROR use2(%s)\n", c);
	if (memcmp(dest, dest2, n) != 0)
		printf("ERROR use3(%s %zu ||| \n%s ||| \n%s)\n", c, n, (char*)dest, (char*)dest2);
	free(dest);
	free(dest2);
}

int	main()
{
	test("test", sizeof ("test"));
	test("", sizeof ("test"));
	test("te\0  \t\tst", sizeof ("test"));
	test("+-123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl", sizeof("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl"));
	test("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl", 20);
}
