#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

int	main()
{
	ft_putchar('-');
	ft_putchar('0');
	ft_putchar('\n');

	ft_putchar('O');
	ft_putchar('K');
	ft_putchar('\n');

	ft_putchar('-');
	ft_putchar('1');
	ft_putchar('\n');

	ft_putstr("OK\n");

	ft_putchar('-');
	ft_putchar('2');
	ft_putchar('\n');

	ft_putendl("OK");

	ft_putchar('-');
	ft_putchar('-');
	ft_putchar('\n');

	ft_putchar_fd('-', 1);
	ft_putchar_fd('0', 1);
	ft_putchar_fd('\n', 1);

	ft_putchar_fd('O', 1);
	ft_putchar_fd('K', 1);
	ft_putchar_fd('\n', 1);

	ft_putchar_fd('-', 1);
	ft_putchar_fd('1', 1);
	ft_putchar_fd('\n', 1);

	ft_putstr_fd("OK\n", 1);

	ft_putchar_fd('-', 1);
	ft_putchar_fd('2', 1);
	ft_putchar_fd('\n', 1);

	ft_putendl_fd("OK", 1);
}
