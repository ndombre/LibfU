#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

void	test(int c, char* t)
{
	char* r = ft_itoa(c);
	if (!ft_strequ(r, t))
		printf("ERROR %s != %s\n", t, r);
	free(r);
}

int	main()
{
	test(0, "0");
	test(1, "1");
	test(-1, "-1");
	test(2147483647, "2147483647");
	test(-2147483648, "-2147483648");
}
