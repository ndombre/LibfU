#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

unsigned int i;
char *s;

void	ft_striter_test(char *str)
{
	if (*str != s[i])
		printf("ERROR\n");
	i++;
}
void	ft_striteri_test(unsigned int a, char *str)
{
	if (*str != s[i])
		printf("ERROR\n");
	if (i != a)
		printf("ERROR\n");
	i++;
}
char	ft_strmap_test(char str)
{
	if (str != s[i])
		printf("ERROR\n");
	i++;
	return ((char)ft_toupper((int)str));
}
char	ft_strmapi_test(unsigned int a, char str)
{
	if (str != s[i])
		printf("ERROR\n");
	if (i != a)
		printf("ERROR\n");
	i++;
	return ((char)ft_toupper((int)str));
}

void	test(char* str, char* str2)
{
	i = 0;
	s = str;
	ft_striter(str, ft_striter_test);
	if (i != ft_strlen(str))
		printf("ERROR\n");
	i = 0;
	ft_striteri(str, ft_striteri_test);
	if (i != ft_strlen(str))
		printf("ERROR\n");
	i = 0;
	char *aa = ft_strmap(str, ft_strmap_test);
	if (ft_strcmp(aa, str2) != 0)
		printf("ERROR\n");
	if (i != ft_strlen(str))
		printf("ERROR\n");
	i = 0;
	char *bb = ft_strmapi(str, ft_strmapi_test);
	if (ft_strcmp(bb, str2) != 0)
		printf("ERROR\n");
	if (i != ft_strlen(str))
		printf("ERROR\n");
	free(aa);
	free(bb);
}

int	main()
{
	test("bonjour !!", "BONJOUR !!");
	test("bonJour ?.!!", "BONJOUR ?.!!");
}
