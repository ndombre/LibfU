/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strsplit.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 15:06:33 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 11:29:20 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int	main(void)
{
	int i;
	char **r = ft_strsplit("*salut***les*gens*", '*');
	
	i=0;
	if (strcmp(r[i++], "salut") != 0) printf("error");
	if (strcmp(r[i++], "les") != 0) printf("error");
	if (strcmp(r[i++], "gens") != 0) printf("error");
	if (r[i++] != 0) printf("error2");

	i=0;
	while (r[i] != 0)
	{
		free(r[i]);
		i++;
	}
	free(r);
}
