#include "../libft.h"
#include <string.h>
#include <stdio.h>

int	main()
{
	char str[] = "memmove can be very useful......";
	ft_memmove (str+20,str+15,11);
	if (strcmp(str, "memmove can be very very useful.") != 0)
		printf("ERROR\n");
	return 0;
}
