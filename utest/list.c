/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 18:32:12 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 11:34:10 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>
#include <stdlib.h>

int g_cont = 0;

void	test_iter(t_list *lis)
{
	if (g_cont == 0)
	{
		if (!ft_strequ(lis->content, "bonjour"))
			printf("error10 %s\n", lis->content);
	}
	else if (g_cont == 1)
	{
		if (!ft_strequ(lis->content, "test"))
			printf("error11 %s\n", lis->content);
	}
	else
		printf("error");
	g_cont++;
}

void	df(void* pt, size_t s)
{
	if (ft_strlen((char*)pt) + 1 != s)
		printf("error139\n");
	g_cont--;
	if (g_cont == 0)
	{
		if (!ft_strequ((char*)pt, "bonjour"))
			printf("error109 %s\n", (char*)pt);
	}
	else if (g_cont == 1)
	{
	if (!ft_strequ((char*)pt, "test"))
		printf("error119 %s\n", (char*)pt);
	}
	else
		printf("error");
	free(pt);
}

int		main(void)
{
	t_list	*l;

	l =  ft_lstnew("test", ft_strlen("test") + 1);
	if (l->next != NULL || ft_strequ(l->content, "test") != 1 || l->content_size != 1 + ft_strlen("test"))
		printf("error4-5-6\n");
	g_cont = 0;
	ft_lstadd(&l,  ft_lstnew("bonjour", ft_strlen("bonjour") + 1));
	ft_lstiter(l, &test_iter);
	ft_lstdel(&l, df);
	if (l != NULL || g_cont != 0)
		printf("error12\n");
}
