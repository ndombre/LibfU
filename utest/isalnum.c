#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

void	test(int c)
{
	if ((isalnum(c) != 0) != (ft_isalnum(c) != 0))
		printf("ERROR use(%d|%d:%d)\n", isalnum(c), ft_isalnum(c), c);
}

int	main()
{
	int i = -5;
	while (i <= 255)
	{
		test(i);
		i++;
	}
}
