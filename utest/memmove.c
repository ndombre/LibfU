#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *c, int set, size_t n)
{
	char *t1 = strdup(c);
	char *t2 = strdup(c);

	if (t1 != memmove(t1, c, n))
		printf("ERROR\n");
	if (t2 != ft_memmove(t2, c, n))
		printf("ERROR\n");
	if (strcmp(t1, t2) != 0)
		printf("ERROR use(%s|%s:%s)\n", t1, t2, c);
	if (memcmp(t1, t2, n) != 0)
		printf("ERROR use(%s|%s:%s)\n", t1, t2, c);
	free(t1);
	free(t2);
}

void	test2(char *c, int d, size_t n)
{
	char *a = malloc(1000);
	char *b = malloc(1000);

	memset(a, '-', 1000);
	memset(b, '-', 1000);

	memcpy(a + 500, c, n);
	memcpy(b + 500, c, n);

	memmove(a + 500, a + d + 500, n);
	ft_memmove(b + 500, b + d + 500, n);

	if (memcmp(a, b, 1000) != 0)
	{
		printf("error\n");
		printf("%s\n", a + 500 + d);
		printf("%s\n", b + 500 + d);
	}
	
	free(a);
	free(b);
}

int	main()
{
	test("almost every programmer should know memset !!!", '-', 6);
	test("almost every programmer should know memset !!!", 2, 6);
	test("almost every programmer should know memset !!!", 2, 0);
	test("almost every programmer should know memset !!!", 'a', sizeof("almost every programmer should know memset !!!"));
	test("a", '\0', sizeof("a"));
	test("Hello !!", 1024, sizeof("Hello !!"));
	test("Hello !!", -5, sizeof("Hello !!"));

	test2("bonjour", -3, 20);
	test2("bonjour", 3, 20);
	test2("bonjour1234567890", -15, 20);
	test2("bonjour1234567890", 15, 20);
}
