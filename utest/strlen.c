#include "../libft.h"
#include "string.h"
#include <stdio.h>

void	test(const char *c)
{
	if (ft_strlen(c) != strlen(c))
		printf("ERROR use(%lu|%lu:%s)\n", strlen(c), ft_strlen(c), c);
}

int	main()
{
	test("");
	test("test");
	test("test42test42testtesttest424242testtesttesttesttest4222222testtest4242\t\vfds");
}
