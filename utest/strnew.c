/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memalloc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:38:32 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 11:29:37 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>
#include <stdlib.h>

int		main(void)
{
	int i = 0;
	char *str = ft_strnew(5);
	if (i < 5)
		str[i] = 5;
	free(str);
	i = 0;
	str = ft_strnew(5);
	if (i < 5)
	{
		if (str[i] != 0)
			printf("error2");
	}
	free(str);
}
