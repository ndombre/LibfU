/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HARDatoi.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 12:49:20 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/09 12:59:29 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdlib.h>
#include <stdio.h>

int	main(void)
{
	char t[4] = {0};
	int i, j, k, r;
	for (i=0; i< 127; i++)
	{
		t[0] = i;
		for (j=0; j < 127; j++)
		{
			t[1] = j;
			for (k=0; k < 127; k++)
			{
				t[2] = k;
				if ((r = ft_atoi(t)) != atoi(t))
				{
					printf("error : ft_atoi = %d atoi = %d str = (%d, %d, %d)\n",r, atoi(t), i, j, k);
				}
			}
		}
	}
}
