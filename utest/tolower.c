#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

void	test(int c)
{
	if (toupper(c) != ft_toupper(c))
		printf("ERROR use(%d|%d:%d)\n", toupper(c), ft_toupper(c), c);
}

int	main()
{
	int i = -1;
	while (i <= 255)
	{
		test(i);
		i++;
	}
}
