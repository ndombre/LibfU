/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strsub.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 09:07:10 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 11:28:58 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int	main(void)
{
	char *a = ft_strsub("b\0onjour !! comment alez vous !", 1, 10);
	if (memcmp(a, "\0onjour !!\0", 11) != 0)
		printf("error\n");
	free(a);
}
