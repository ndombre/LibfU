/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 18:32:12 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 11:36:09 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int g_cont = 0;

t_list	*test_iter(t_list *lis)
{
	if (strcmp(lis->content, "bonjour")==0 && g_cont == 0)
	{
		g_cont++;
		return (ft_lstnew("tt", 3));
	}
	if (strcmp(lis->content, "test")==0 && g_cont == 1)
	{
		g_cont++;
		return (NULL);
	}
	printf("error too many call %s %d\n", lis->content, g_cont);
	return (NULL);
}

void	df(void* pt, size_t s)
{
	g_cont--;
	free(pt);
}

int		main(void)
{
	t_list	*l;

	l =  ft_lstnew("test", ft_strlen("test") + 1);
	ft_lstadd(&l,  ft_lstnew("bonjour", ft_strlen("bonjour") + 1));
	t_list *l2 = ft_lstmap(l, &test_iter);
	if (l2 != NULL)
		printf("error NULL\n");
	ft_lstdel(&l, df);
	if (l != NULL || g_cont != 0)
		printf("error12\n");
}
