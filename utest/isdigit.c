#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

void	test(int c)
{
	if ((isdigit(c)!=0) != (ft_isdigit(c)!=0))
		printf("ERROR use(%d|%d:%d)\n", isdigit(c), ft_isdigit(c), c);
}

int	main()
{
	int i = -5;
	while (i <= 255)
	{
		test(i);
		i++;
	}
}
