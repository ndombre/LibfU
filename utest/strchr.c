#include "../libft.h"
#include "string.h"
#include <stdio.h>

void	test(const char *c, int c2)
{
	if (strchr(c, c2) != ft_strchr(c, c2))
		printf("ERROR use(%s|%s:%s|%d)\n", strchr(c, c2), ft_strchr(c, c2), c, c2);
}

int	main()
{
	test("", '\0');
	test("tt", '\0');
	test("test", 't');
	test("test", 's');
	test("test", 'y');
	test("test42test42testtesttest424242testtesttesttesttest4222222testtest4242\t\vfds", '\t');

	test("", '1');
	test("1", '1');
}
