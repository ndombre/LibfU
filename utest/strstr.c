/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/11 18:43:58 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 16:22:56 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include "../libft.h"

void find_str(char* str, char* substr) 
{
	char* pos = ft_strstr(str, substr);
	char* posreel = strstr(str, substr);
	if (pos != posreel)
	{
		if(pos) {
	    		printf("ERROR found the string '%s' in '%s' at position: %ld\n", substr, str, pos - str);
		} else {
			printf("ERROR the string '%s' was not found in '%s'\n", substr, str);
		}
	}
}
 
int main(void) 
{
	char* str = "one two three";
	find_str(str, "two");
	find_str(str, "");
	find_str(str, "nine");
	find_str(str, "n");
	find_str(str, "y");
	find_str(str, "one two threeeee");
	find_str("", "");
	find_str("ewqesdsdfdfds", "");
	find_str("", "fdsfdsfdsfdsf");
	find_str("fdshfgdh\200fdsfdsfds", "\200");

	find_str("Ceci n'est pas er bvder.", ".");
	return 0;
}
