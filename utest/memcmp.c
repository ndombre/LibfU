/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memcmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 19:34:07 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/07 19:42:01 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>
#include <string.h>

int		main()
{
	if (ft_memcmp("test", "test", strlen("test")) != 0)
		printf("error");
	if (memcmp("tes1", "test", strlen("test")) != ft_memcmp("tes1", "test", strlen("test")))
		printf("error");
}
