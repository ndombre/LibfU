/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MYstrcontif.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 12:51:32 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 13:05:10 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>

int		main(void)
{
	if (ft_strtest("bonjour", ft_islower) != 1)
		printf("error");
	if (ft_strtest("bonjoTr", ft_islower) != 0)
		printf("error");
	return (0);
}
