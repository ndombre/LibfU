#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

void	test(int c)
{
	if ((isalpha(c) != 0) != (ft_isalpha(c) != 0))
		printf("ERROR use(%d|%d:%d)\n", isalpha(c), ft_isalpha(c), c);
}

int	main()
{
	int i = -5;
	while (i <= 255)
	{
		test(i);
		i++;
	}
}
