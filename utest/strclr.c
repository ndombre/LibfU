/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strclr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:23:06 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/07 14:30:30 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft.h"
#include <stdio.h>

int		main(void)
{
	int i = 0;
	char test[] = "bonjour";
	char test2[] = "bonjour";

	ft_strclr(test2);
	while (test[i] != '\0')
	{
		if (test2[i] != '\0')
			printf("error\n");
		i++;
	}
}
