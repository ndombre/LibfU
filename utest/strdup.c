#include "../libft.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	test(char *c)
{
	char *dest = ft_strdup(c);
	if (strcmp(dest, c) != 0)
		printf("ERROR use(%s)\n", c);
	free(dest);
}

int	main()
{
	test("test");
	test("");
	test("te\0  \t\tst");
	test("--123456789hjngfhlkdfjgkldsnjklvfndsjlhfjkhlJHFJHFDLSH:LHLFHJHFLH:DFDSDOFHHJGFLDS:Hfjfdklshfjl");
}
