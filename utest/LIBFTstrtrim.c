#include "../libft.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

void	test(char *c, char* t)
{
	char* r = ft_strtrim(c);
	if (!ft_strequ(r, t))
		printf("ERROR(%d) '%s' != '%s'\n", c[0], t, r);
	free(r);
}

int	main()
{
	test("  0   ", "0");
	test("1", "1");
	test("-1\t\n", "-1");
	test("     ", "");
	test("-2147483648", "-2147483648");
	int i;
	char t[100] = "1234567890";
	for (i = 0; i < 127; i++)
	{
		t[0] = i;
		if (i != ' ' && i != '\n' && i != '\t')
			test(t, t);
		else
			test(t, t + 1);
	}
}
