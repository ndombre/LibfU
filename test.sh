DIR=$1

echo currant execution path $DIR

MODE="MY"

#commenter la ligne !!
MODE="LIBFT"

norminette $DIR/*.c | grep -E '^(Error|Warning)'
norminette $DIR/*.h | grep -E '^(Error|Warning)'
make -C $DIR

LIBFT_A=libft.a
LIBFT_H=libft.h

cp $DIR$LIBFT_A .
cp $DIR$LIBFT_H .

nm libft.a | grep " U " | grep -v "ft_" | grep -v "write" | grep -v "free" | grep -v "malloc"

for file in $(ls utest/ | grep -v $MODE | sort)
do
echo ====================== run utest/$file ==========================
gcc -L. ./utest/$file ./libft.a
./a.out
done
rm a.out
rm libft.a 2> /dev/null
rm libft.h 2> /dev/null

echo "normalemt, se qui est afficher est equivalent a ref.txt"
